package fr.ign.vsasyan.twosidesofthecoin;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Random;

public class CoinActivity extends AppCompatActivity {

    /**
     * Déclaration des objets Java représentant les composants graphiques
     * (en attributs de la classe, ils sont donc accessibles depuis toutes les méthodes)
     */
    TextView tv_coinResult;
    Button b_coin_flip;

    /**
     * Fonction exécutée à la création de la vue
     * Elle DOIT instancier les objets Java représentant les composants
     * graphiques ET leur ajouter des écouteurs d'événements si besoin.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coin);

        // I - Instanciation les objets Java représentant les composants graphiques
        tv_coinResult = (TextView)findViewById(R.id.tv_coin_result);
        b_coin_flip = (Button)findViewById(R.id.b_coin_flip);

        // II - Ajout des écouteurs d'événements aux composants graphiques représentés par des objets Java

        // Ajout d'un écouteur d'événement "OnClickListener" anonyme à l'objet "Button" b_coin_flip représentant le composant "Button" "b_coin_flip"
        b_coin_flip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Here we have the function that flip the coin
                CoinActivity.this.coinFlip();
            }
        });

        // III - Autre traitement à effectuer au début...
        coinFlip();
    }

    protected  void coinFlip() {
        // Creation of a randomGenerator
        Random randomGenerator = new Random();

        // Tail or not Tail that is the question?
        boolean tail = randomGenerator.nextBoolean();

        // Put the result in a string
        String result;
        if (tail) {
            result = getResources().getString(R.string.tail);
        } else {
            result = getResources().getString(R.string.head);
        }

        // Set the result
        String oldText = tv_coinResult.getText().toString();
        String newText = result + System.getProperty("line.separator") + oldText;
        tv_coinResult.setText(newText);
    }
}
